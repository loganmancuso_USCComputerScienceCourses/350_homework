/****************************************************************
 * 'program2.h'
 *
 * Author/CopyRight: Mancuso, Logan
 * Last Edit Date: 04-03-2018--20:33:49
 * 
 *
**/

#ifndef PROGRAM2_H
#define PROGRAM2_H

#include <algorithm>
#include "metrics.h"
#include <cmath>
#include <stdlib.h>


/****************************************************************
 * Function 'closestPair'
 * algorithm to find the closest pair in a list. 
 * Runs O(n*log(n))
 * 	if n <= 3 brute force it
 *	else
 *	copy ceil(n/2) p to array pleft
 *	copy ceil(n/2) of q to qleft
 *	copy remaining floor(n/2) p to pright
 *	copy remaining floor(n/2) q to qright
 *	dleft = closestPair( pleft, qleft )
 *	dright = closestPair( pright, qright )
 *	distance = min( dleft, dright )
 *	m = P(ceil(n/2)-1).x
 *	copy all the points of q for which abs(x-m) < d into array S[0..num-1]
 *	dminsq = d^2
 *	for i to num - 2 do 
 *		k i++
 *	 	while k <= num - 1 and (S[k].y -S[i].y)^2 < dminsq
 *	 		dminsq = min(S[k].x - S[i].x)^2 + (S[k].y-S[i].y)^2, dminsqr)
 *	 		k ++
 *	 return sqrt(dminsq)
**/
template <typename T>
std::pair<Point<T>, Point<T> > closestPair( vector<Point<T> > v) {
	typename std::vector<Point<T>>::iterator iter;
	//vectors of points to store left and right values from v
	std::vector<Point<T>> x_left;
	std::vector<Point<T>> x_right;
	std::vector<Point<T>> y_left;
	std::vector<Point<T>> y_right;
	

	//left half of points
	for( iter = v.begin(); !iter.equals(ceil(v.size()/2)); ++iter ) {
		x_left.push_back(iter.x);
		y_left.push_back(iter.y);
		printf("test %s", iter);
	}//end for
	//right half of points
	for( iter = v.end(); !iter.equals(floor(v.size()/2)); --iter ) {
		x_right.push_back(iter.x);
		y_right.push_back(iter.y);
		printf("test %s", iter);
	}//end for



	return std::pair<Point<T>, Point<T> > (v[0],v[1]);

}//end closestPair

#endif //PROGRAM2_H

/****************************************************************
 * End 'program2.h'
**/
